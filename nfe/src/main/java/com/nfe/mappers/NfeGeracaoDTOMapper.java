package com.nfe.mappers;

import com.nfe.models.Nfe;
import com.nfe.models.dtos.NfeGeracaoKafka;

public class NfeGeracaoDTOMapper {

    public static Nfe fromNfeGeracaoKafka(NfeGeracaoKafka nfeGeracaoKafka){
        Nfe nfe = new Nfe();
        nfe.setValorInicial(nfeGeracaoKafka.getValorInicial());
        nfe.setValorFinal(nfeGeracaoKafka.getValorFinal());
        nfe.setValorIRRF(nfeGeracaoKafka.getValorIRRF());
        nfe.setValorCSLL(nfeGeracaoKafka.getValorCSLL());
        nfe.setValorCofins(nfeGeracaoKafka.getValorCofins());
        return nfe;
    }
}
