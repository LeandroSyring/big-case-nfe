package com.nfe.mappers;

import com.nfe.models.NfeSolicitacao;
import com.nfe.models.dtos.NfeConsultaSolicitacaoResponse;
import com.nfe.models.dtos.NfeSolicitacaoKafka;
import com.nfe.models.dtos.NfeSolicitacaoRequest;

public class NfeSolicitacaoDTOMapper {

    public static NfeSolicitacaoKafka toNfeSolicitacaoKafka(NfeSolicitacao nfeSolicitacao){
        NfeSolicitacaoKafka nfeSolicitacaoKafka = new NfeSolicitacaoKafka();
        nfeSolicitacaoKafka.setIdentidade(nfeSolicitacao.getIdentidade());
        nfeSolicitacaoKafka.setValor(nfeSolicitacao.getValor());
        nfeSolicitacaoKafka.setStatus(nfeSolicitacao.getStatus());
        nfeSolicitacaoKafka.setIdSolicitacao(nfeSolicitacao.getId());
        return nfeSolicitacaoKafka;
    }

    public static NfeSolicitacao fromNfeSolicitacaoRequest(NfeSolicitacaoRequest nfeSolicitacaoRequest){
        NfeSolicitacao nfeSolicitacao = new NfeSolicitacao();
        nfeSolicitacao.setIdentidade(nfeSolicitacaoRequest.getIdentidade());
        nfeSolicitacao.setValor(nfeSolicitacaoRequest.getValor());
        return nfeSolicitacao;
    }

    public static NfeConsultaSolicitacaoResponse fromNfeSolicitacao(NfeSolicitacao nfeSolicitacao){

        NfeConsultaSolicitacaoResponse nfeConsultaSolicitacaoResponse = new NfeConsultaSolicitacaoResponse();
        nfeConsultaSolicitacaoResponse.setIdentidade(nfeSolicitacao.getIdentidade());
        nfeConsultaSolicitacaoResponse.setValor(nfeSolicitacao.getValor());
        nfeConsultaSolicitacaoResponse.setStatus(nfeSolicitacao.getStatus());
        nfeConsultaSolicitacaoResponse.setNfe(nfeSolicitacao.getNfe());

        return nfeConsultaSolicitacaoResponse;
    }
}
