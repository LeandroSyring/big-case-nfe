package com.nfe.repositories;

import com.nfe.models.NfeSolicitacao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NfeSolicitacaoRepository extends CrudRepository<NfeSolicitacao, Integer> {
    List<NfeSolicitacao> findByIdentidade(String identidade);
}
