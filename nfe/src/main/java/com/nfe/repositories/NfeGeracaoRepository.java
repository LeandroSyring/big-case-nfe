package com.nfe.repositories;

import com.nfe.models.Nfe;
import org.springframework.data.repository.CrudRepository;

public interface NfeGeracaoRepository extends CrudRepository<Nfe, Integer> {
}
