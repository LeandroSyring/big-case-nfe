package com.nfe.controllers;

import com.nfe.enums.NfeTipoStatus;
import com.nfe.mappers.NfeSolicitacaoDTOMapper;
import com.nfe.models.NfeSolicitacao;
import com.nfe.models.dtos.NfeConsultaSolicitacaoResponse;
import com.nfe.models.dtos.NfeSolicitacaoRequest;
import com.nfe.models.dtos.NfeSolicitacaoResponse;
import com.nfe.services.NfeSolicitacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/nfe")
public class NfeSolicitacaoController {

    @Autowired
    private NfeSolicitacaoService nfeSolicitacaoService;

    @PostMapping("/emitir")
    public ResponseEntity<NfeSolicitacaoResponse> solicitarEmissaoNfe(@RequestBody NfeSolicitacaoRequest nfeSolicitacaoRequest){

        NfeSolicitacao nfeSolicitacao = NfeSolicitacaoDTOMapper.fromNfeSolicitacaoRequest(nfeSolicitacaoRequest);
        NfeTipoStatus status = nfeSolicitacaoService.enviarSolicitacaoNfe(nfeSolicitacao);
        NfeSolicitacaoResponse nfeSolicitacaoResponse = new NfeSolicitacaoResponse();
        nfeSolicitacaoResponse.setStatus(status);
        return ResponseEntity.status(201).body(nfeSolicitacaoResponse);
    }

    @GetMapping("/consultar/{identidade}")
    public ResponseEntity<List<NfeConsultaSolicitacaoResponse>> buscarSolicitacaoPorIdentidade(@PathVariable String identidade){

        List<NfeSolicitacao> nfeSolicitacaoList = nfeSolicitacaoService.consultarNfePorIdentidade(identidade);

        List<NfeConsultaSolicitacaoResponse> nfeConsultaSolicitacaoResponseList = new ArrayList<NfeConsultaSolicitacaoResponse>();
        for (NfeSolicitacao nfeSolicitacao : nfeSolicitacaoList) {
            nfeConsultaSolicitacaoResponseList.add(NfeSolicitacaoDTOMapper.fromNfeSolicitacao(nfeSolicitacao));
        }
        
        return ResponseEntity.status(200).body(nfeConsultaSolicitacaoResponseList);
    }

}
