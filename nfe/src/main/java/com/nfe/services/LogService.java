package com.nfe.services;

import com.nfe.models.dtos.LogKafka;
import com.nfe.producers.LogProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class LogService {

    @Autowired
    private LogProducer logProducer;

    public void enviarLogDoSistema(Date data, String acao, String log){

        String logMontado = "[" + data + "]" + "] [" + acao + "]: " + log;
        LogKafka logKafka = new LogKafka();
        logKafka.setLog(logMontado);
        logProducer.enviarAokafka(logKafka);
    }
}
