package com.nfe.services;

import com.nfe.enums.NfeTipoStatus;
import com.nfe.exceptions.IdentidadeBadRequestException;
import com.nfe.exceptions.NfeSolicitacaoNotFoundException;
import com.nfe.mappers.NfeSolicitacaoDTOMapper;
import com.nfe.models.Nfe;
import com.nfe.models.NfeSolicitacao;
import com.nfe.producers.NfeProducer;
import com.nfe.repositories.NfeSolicitacaoRepository;
import com.nfe.utils.ValidCpfCnpj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class NfeSolicitacaoService {

    @Autowired
    private NfeSolicitacaoRepository nfeSolicitacaoRepository;

    @Autowired
    private NfeProducer nfeProducer;

    @Autowired
    private LogService logService;

    @NewSpan(name = "nfe-solicitacao-service")
    public NfeTipoStatus enviarSolicitacaoNfe(NfeSolicitacao nfeSolicitacao){

        if(ValidCpfCnpj.isValid(nfeSolicitacao.getIdentidade())) {
            nfeSolicitacao.setStatus(NfeTipoStatus.pending);
            NfeSolicitacao nfeSolicitacaoSalvo = this.incluirSolicitacaoNfe(nfeSolicitacao);

            nfeProducer.enviarAoKafka(NfeSolicitacaoDTOMapper.toNfeSolicitacaoKafka(nfeSolicitacaoSalvo));


            Date dataAtual = new Date();
            String mensagem = nfeSolicitacao.getIdentidade() + " acaba de pedir a emissão de uma NF no valor de R$ " + nfeSolicitacao.getValor() + "!";
            logService.enviarLogDoSistema(dataAtual, "Emissão", mensagem);
            return nfeSolicitacao.getStatus();
        }
        else{
            throw new IdentidadeBadRequestException();
        }
    }

    public NfeSolicitacao buscarSolicitacao(Integer id){
        Optional<NfeSolicitacao> nfeSolicitacaoOptional = nfeSolicitacaoRepository.findById(id);
        if(nfeSolicitacaoOptional.isPresent()){
            return nfeSolicitacaoOptional.get();
        }
        throw new NfeSolicitacaoNotFoundException();
    }


    public NfeSolicitacao incluirSolicitacaoNfe(NfeSolicitacao nfeSolicitacao){
        return nfeSolicitacaoRepository.save(nfeSolicitacao);
    }

    public void atualizarGeracaoNfe(Integer idSolicitacao, Nfe nfe, NfeTipoStatus statusSolicitacao){

        NfeSolicitacao nfeSolicitacao = this.buscarSolicitacao(idSolicitacao);

        nfeSolicitacao.setStatus(statusSolicitacao);

        nfeSolicitacao.setNfe(nfe);

        nfeSolicitacaoRepository.save(nfeSolicitacao);
    }

    public List<NfeSolicitacao> consultarNfePorIdentidade(String identidade){

        List<NfeSolicitacao> nfeSolicitacaoList = nfeSolicitacaoRepository.findByIdentidade(identidade);

        if(!nfeSolicitacaoList.isEmpty()){
            return nfeSolicitacaoList;
        }
        throw new NfeSolicitacaoNotFoundException();
    }
}
