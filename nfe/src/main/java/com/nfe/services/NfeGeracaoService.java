package com.nfe.services;

import com.nfe.mappers.NfeGeracaoDTOMapper;
import com.nfe.models.Nfe;
import com.nfe.models.dtos.NfeGeracaoKafka;
import com.nfe.repositories.NfeGeracaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class NfeGeracaoService {

    @Autowired
    private NfeGeracaoRepository nfeGeracaoRepository;

    @Autowired
    private NfeSolicitacaoService nfeSolicitacaoService;

    @Autowired
    private LogService logService;

    @NewSpan(name = "nfe-geracao-service")
    public void gerarNfe(NfeGeracaoKafka nfeGeracaoKafka){

        Nfe nfe = nfeGeracaoRepository.save(NfeGeracaoDTOMapper.fromNfeGeracaoKafka(nfeGeracaoKafka));

        nfeSolicitacaoService.atualizarGeracaoNfe(
                nfeGeracaoKafka.getIdSolicitacao(),
                nfe,
                nfeGeracaoKafka.getStatus());

        Date dataAtual = new Date();
        String mensagem = "Nota fiscal Nro " + nfe.getId() + " foi salva! Identidade: " + nfeGeracaoKafka.getIdentidade() + " e valor final: R$ + " + nfe.getValorFinal() + " !";
        logService.enviarLogDoSistema(dataAtual, "Salvar Emissão", mensagem);

    }
}
