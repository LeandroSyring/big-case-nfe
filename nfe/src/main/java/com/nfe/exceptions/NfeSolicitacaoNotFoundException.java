package com.nfe.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Solicitacao não encontrada!")
public class NfeSolicitacaoNotFoundException extends RuntimeException{
}
