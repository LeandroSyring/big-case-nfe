package com.nfe.producers;

import com.nfe.models.dtos.NfeSolicitacaoKafka;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class NfeProducer {

    @Autowired
    private KafkaTemplate<String, NfeSolicitacaoKafka> producer;

    public void enviarAoKafka(NfeSolicitacaoKafka nfeSolicitacaoKafka){
        producer.send("spec2-leandro-guilherme-1", nfeSolicitacaoKafka);
    }
}
