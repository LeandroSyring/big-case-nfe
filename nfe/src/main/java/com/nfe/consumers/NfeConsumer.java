package com.nfe.consumers;

import com.nfe.models.dtos.NfeGeracaoKafka;
import com.nfe.services.NfeGeracaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class NfeConsumer {

    @Autowired
    private NfeGeracaoService nfeGeracaoService;

    @KafkaListener(topics = "spec2-leandro-guilherme-2", groupId = "grupo-1")
    public void receber(@Payload NfeGeracaoKafka nfeGeracaoKafka){
        nfeGeracaoService.gerarNfe(nfeGeracaoKafka);
        System.out.println(nfeGeracaoKafka.getIdentidade());
    }
}
