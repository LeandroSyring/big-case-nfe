package com.nfe.models;

import com.nfe.enums.NfeTipoStatus;

import javax.persistence.*;

@Entity
public class NfeSolicitacao {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    private String identidade;

    private double valor;

    private NfeTipoStatus status;

    @OneToOne
    private Nfe nfe;

    public NfeSolicitacao() {
    }

    public NfeSolicitacao(Integer id, String identidade, double valor, NfeTipoStatus status, Nfe nfe) {
        this.id = id;
        this.identidade = identidade;
        this.valor = valor;
        this.status = status;
        this.nfe = nfe;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public NfeTipoStatus getStatus() {
        return status;
    }

    public void setStatus(NfeTipoStatus status) {
        this.status = status;
    }

    public Nfe getNfe() {
        return nfe;
    }

    public void setNfe(Nfe nfe) {
        this.nfe = nfe;
    }
}
