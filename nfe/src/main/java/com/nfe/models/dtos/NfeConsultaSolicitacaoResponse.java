package com.nfe.models.dtos;

import com.nfe.enums.NfeTipoStatus;
import com.nfe.models.Nfe;

public class NfeConsultaSolicitacaoResponse {

    private String identidade;

    private double valor;

    private NfeTipoStatus status;

    private Nfe nfe;

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public NfeTipoStatus getStatus() {
        return status;
    }

    public void setStatus(NfeTipoStatus status) {
        this.status = status;
    }

    public Nfe getNfe() {
        return nfe;
    }

    public void setNfe(Nfe nfe) {
        this.nfe = nfe;
    }
}
