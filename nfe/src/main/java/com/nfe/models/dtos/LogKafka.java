package com.nfe.models.dtos;

public class LogKafka {

    String log;

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}
