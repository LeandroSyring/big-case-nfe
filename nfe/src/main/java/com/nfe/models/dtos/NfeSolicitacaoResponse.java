package com.nfe.models.dtos;

import com.nfe.enums.NfeTipoStatus;

public class NfeSolicitacaoResponse {

    private NfeTipoStatus status;

    public NfeTipoStatus getStatus() {
        return status;
    }

    public void setStatus(NfeTipoStatus status) {
        this.status = status;
    }
}
