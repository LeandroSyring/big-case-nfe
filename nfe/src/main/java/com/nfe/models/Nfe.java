package com.nfe.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Nfe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private double valorInicial;

    private double valorIRRF;

    private double valorCSLL;

    private double valorCofins;

    private double valorFinal;

    public Nfe() {
    }

    public Nfe(Integer id, double valorInicial, double valorIRRF, double valorCSLL, double valorCofins, double valorFinal) {
        this.id = id;
        this.valorInicial = valorInicial;
        this.valorIRRF = valorIRRF;
        this.valorCSLL = valorCSLL;
        this.valorCofins = valorCofins;
        this.valorFinal = valorFinal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getValorInicial() {
        return valorInicial;
    }

    public void setValorInicial(double valorInicial) {
        this.valorInicial = valorInicial;
    }

    public double getValorIRRF() {
        return valorIRRF;
    }

    public void setValorIRRF(double valorIRRF) {
        this.valorIRRF = valorIRRF;
    }

    public double getValorCSLL() {
        return valorCSLL;
    }

    public void setValorCSLL(double valorCSLL) {
        this.valorCSLL = valorCSLL;
    }

    public double getValorCofins() {
        return valorCofins;
    }

    public void setValorCofins(double valorCofins) {
        this.valorCofins = valorCofins;
    }

    public double getValorFinal() {
        return valorFinal;
    }

    public void setValorFinal(double valorFinal) {
        this.valorFinal = valorFinal;
    }
}
