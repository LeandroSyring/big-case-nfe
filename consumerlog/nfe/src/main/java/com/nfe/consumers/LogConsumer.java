package com.nfe.consumers;

import com.nfe.models.dtos.LogKafka;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class LogConsumer {

    @NewSpan(name = "nfe-log-consumer")
    @KafkaListener(topics = "spec2-leandro-guilherme-3", groupId = "grupo-1")
    public void receber(@Payload LogKafka logKafka) throws IOException {
        FileWriter arq = new FileWriter("logs_nfe.csv", true);
        PrintWriter gravarArq = new PrintWriter(arq);
        gravarArq.println(logKafka.getLog());
        arq.close();

        System.out.println(logKafka.getLog());
    }
}
