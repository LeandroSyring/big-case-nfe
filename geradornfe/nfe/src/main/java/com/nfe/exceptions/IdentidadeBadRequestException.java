package com.nfe.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Identificação Inválida.")
public class IdentidadeBadRequestException extends RuntimeException{
}
