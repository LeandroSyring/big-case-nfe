package com.nfe.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NO_CONTENT, reason = "Cliente não encontrado!")
public class EmpresaGovernoClientNotFoundException extends RuntimeException{
}
