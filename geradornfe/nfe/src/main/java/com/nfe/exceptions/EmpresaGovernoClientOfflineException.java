package com.nfe.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Serviço de Empresa Governo Offline")
public class EmpresaGovernoClientOfflineException extends RuntimeException {
}
