package com.nfe.mapper;

import com.nfe.models.Nfe;
import com.nfe.models.dtos.NfeGeracaoKafka;

public class NfeGeracaoDTOMapper {

    public static NfeGeracaoKafka toNfeGeracaoKafka (Nfe nfe){
        NfeGeracaoKafka nfeGeracaoKafka = new NfeGeracaoKafka();
        nfeGeracaoKafka.setIdSolicitacao(nfe.getIdSolicitacao());
        nfeGeracaoKafka.setIdentidade(nfe.getIdentidade());
        nfeGeracaoKafka.setStatus(nfe.getStatus());
        nfeGeracaoKafka.setValorCofins(nfe.getValorCofins());
        nfeGeracaoKafka.setValorCSLL(nfe.getValorCSLL());
        nfeGeracaoKafka.setValorIRRF(nfe.getValorIRRF());
        nfeGeracaoKafka.setValorInicial(nfe.getValorInicial());
        nfeGeracaoKafka.setValorFinal(nfe.getValorFinal());

        return nfeGeracaoKafka;
    }
}
