package com.nfe.mapper;

import com.nfe.models.Nfe;
import com.nfe.models.dtos.NfeSolicitacaoKafka;

public class NfeConsumerDTOMapper {

    public static Nfe toNfe(NfeSolicitacaoKafka nfeSolicitacaoKafka){
        Nfe nfe = new Nfe();
        nfe.setIdSolicitacao(nfeSolicitacaoKafka.getIdSolicitacao());
        nfe.setIdentidade(nfeSolicitacaoKafka.getIdentidade());
        nfe.setValorInicial(nfeSolicitacaoKafka.getValor());
        nfe.setStatus(nfeSolicitacaoKafka.getStatus());
        return nfe;
    }
}
