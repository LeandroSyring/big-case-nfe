package com.nfe.models.dtos;

import com.nfe.enums.NfeTipoStatus;

public class NfeSolicitacaoKafka {

    private Integer idSolicitacao;

    private String identidade;

    private double valor;

    private NfeTipoStatus status;

    public Integer getIdSolicitacao() {
        return idSolicitacao;
    }

    public void setIdSolicitacao(Integer idSolicitacao) {
        this.idSolicitacao = idSolicitacao;
    }

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public NfeTipoStatus getStatus() {
        return status;
    }

    public void setStatus(NfeTipoStatus status) {
        this.status = status;
    }
}
