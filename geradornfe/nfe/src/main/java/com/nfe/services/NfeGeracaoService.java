package com.nfe.services;

import com.nfe.client.EmpresaGovernoClient;
import com.nfe.client.EmpresaGovernoDTO;
import com.nfe.enums.NfeTipoStatus;
import com.nfe.exceptions.IdentidadeBadRequestException;
import com.nfe.mapper.NfeGeracaoDTOMapper;
import com.nfe.models.Nfe;
import com.nfe.producers.NfeProducer;
import com.nfe.utils.ValidCpfCnpj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class NfeGeracaoService {

    @Autowired
    private EmpresaGovernoClient empresaGovernoClient;

    @Autowired
    private LogService logService;

    @Autowired
    private NfeProducer nfeProducer;

    @NewSpan(name = "gerador-nfe-service")
    public void gerarNotaFiscal(Nfe nfe){

        if(ValidCpfCnpj.isValid(nfe.getIdentidade())) {

            Nfe nfeAtualizado = this.calcularValoresNotaFiscal(nfe);
            nfe.setStatus(NfeTipoStatus.complete);
            nfeProducer.enviarAoKafka(NfeGeracaoDTOMapper.toNfeGeracaoKafka(nfe));
            Date dataAtual = new Date();
            String mensagem = "Nota Fiscal com valor R$ " + nfeAtualizado.getValorInicial() + " acabou de ser gerada, para a identidade " + nfeAtualizado.getIdentidade() + " !";
            logService.enviarLogDoSistema(dataAtual, "Geração", mensagem);
        }
        else{
            throw new IdentidadeBadRequestException();
        }
    }

    @NewSpan(name = "nfe-calcula-impostos-service")
    public Nfe calcularValoresNotaFiscal(Nfe nfe){

        double valorIRRF = 0;
        double valorCSLL = 0;
        double valorCofins = 0;
        double valorFinal = nfe.getValorInicial();
        double valorInicial = nfe.getValorInicial();

        if(nfe.getIdentidade().length() > 11) {
            EmpresaGovernoDTO empresaGovernoDTO = empresaGovernoClient.pesquisarEmpresaGoverno(nfe.getIdentidade());
            //Empresa optante pelo simples
            if (empresaGovernoDTO.getCapital_social() < 1000000) {
                valorIRRF = 0.015 * valorInicial;
                valorFinal -= valorIRRF;
            } else {//Empresa não optante pelo simples
                valorIRRF = 0.015 * valorInicial;
                valorCSLL = 0.03 * valorInicial;
                valorCofins = 0.0065 * valorInicial;
                valorFinal -= (valorIRRF + valorCSLL + valorCofins);
            }
        }
        nfe.setValorCofins(valorCofins);
        nfe.setValorCSLL(valorCSLL);
        nfe.setValorIRRF(valorIRRF);
        nfe.setValorFinal(valorFinal);

        return nfe;
    }
}
