package com.nfe.producers;

import com.nfe.models.dtos.NfeGeracaoKafka;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class NfeProducer {

    @Autowired
    KafkaTemplate<String, NfeGeracaoKafka> producer;

    public void enviarAoKafka(NfeGeracaoKafka nfeGeracaoKafka){
        producer.send("spec2-leandro-guilherme-2", nfeGeracaoKafka);
    }

}
