package com.nfe.producers;

import com.nfe.models.dtos.LogKafka;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class LogProducer {

    @Autowired
    KafkaTemplate<String, LogKafka> producer;

    public void enviarAokafka(LogKafka logKafka){
        producer.send("spec2-leandro-guilherme-3", logKafka);
    }
}
