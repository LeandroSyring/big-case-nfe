package com.nfe.consumers;

import com.nfe.mapper.NfeConsumerDTOMapper;
import com.nfe.models.dtos.NfeSolicitacaoKafka;
import com.nfe.services.NfeGeracaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class NfeConsumer {

    @Autowired
    private NfeGeracaoService nfeGeracaoService;

    @KafkaListener(topics = "spec2-leandro-guilherme-1", groupId = "grupo-1")
    public void receber(@Payload NfeSolicitacaoKafka nfeSolicitacaoKafka){

        nfeGeracaoService.gerarNotaFiscal(NfeConsumerDTOMapper.toNfe(nfeSolicitacaoKafka));
        System.out.println(nfeSolicitacaoKafka.getIdentidade());
    }
}
