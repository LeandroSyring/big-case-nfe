package com.nfe.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "empresa", url = "https://www.receitaws.com.br/v1/cnpj/")
public interface EmpresaGovernoClient {

    @NewSpan(name = "empresa-governo-client")
    @GetMapping("/{cnpj}")
    EmpresaGovernoDTO pesquisarEmpresaGoverno(@PathVariable String cnpj);
}
