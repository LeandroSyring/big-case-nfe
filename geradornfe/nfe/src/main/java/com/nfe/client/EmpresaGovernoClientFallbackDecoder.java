package com.nfe.client;

import com.nfe.exceptions.EmpresaGovernoClientOfflineException;

import java.io.IOException;

public class EmpresaGovernoClientFallbackDecoder implements EmpresaGovernoClient{


    private Exception cause;

    public EmpresaGovernoClientFallbackDecoder(Exception cause) {
        this.cause = cause;
    }

    @Override
    public EmpresaGovernoDTO pesquisarEmpresaGoverno(String cnpj) {
        if (cause instanceof IOException || cause instanceof RuntimeException){
            throw new EmpresaGovernoClientOfflineException();
        }
        throw (RuntimeException) cause;
    }
}
