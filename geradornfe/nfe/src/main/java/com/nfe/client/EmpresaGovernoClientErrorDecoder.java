package com.nfe.client;

import com.nfe.exceptions.EmpresaGovernoClientNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class EmpresaGovernoClientErrorDecoder implements ErrorDecoder{

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            return new EmpresaGovernoClientNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }
}
