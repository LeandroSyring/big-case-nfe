package com.nfe.client;

public class EmpresaGovernoDTO {

    private double capital_social;

    private String cnpj;

    public EmpresaGovernoDTO(double capital_social, String cnpj) {
        this.capital_social = capital_social;
        this.cnpj = cnpj;
    }

    public EmpresaGovernoDTO() {
    }

    public double getCapital_social() {
        return capital_social;
    }

    public void setCapital_social(double capital_social) {
        this.capital_social = capital_social;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
}
