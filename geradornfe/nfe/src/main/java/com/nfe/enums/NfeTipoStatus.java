package com.nfe.enums;

public enum NfeTipoStatus {
    pending,
    complete
}
