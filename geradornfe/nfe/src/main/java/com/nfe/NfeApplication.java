package com.nfe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class NfeApplication {

	public static void main(String[] args) {
		SpringApplication.run(NfeApplication.class, args);
	}

}
